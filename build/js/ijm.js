$(function() {

    var App = (function(){

        return {
            init : function() {
                IJM.init();

                //DummyModule.init();
            }
        }
    })()

    /**
     * Infrastructure JS Map (IJM) Module
     */
    , IJM = (function(){
        var IJM = {};

        /**
         * Cache jQuery ELement Instances
         */
        IJM.cacheJqueryElemsInstances = function() {
            IJM.$ = {};

            IJM.$.container = $(".ijm");
            IJM.$.controls = IJM.$.container.find(".ijm__control");
            IJM.$.control3D = IJM.$.controls.filter('.ijm__control--3D');
            IJM.$.controlPlain = IJM.$.controls.filter('.ijm__control--plain');

            IJM.$.canvas = IJM.$.container.find(".ijm__canvas");
            IJM.$.canvas3D = IJM.$.canvas.filter('.ijm__canvas--3D');
            IJM.$.canvasPlain = IJM.$.canvas.filter('.ijm__canvas--plain');
            IJM.$.canvasPic3D = IJM.$.canvas3D.find(".ijm__img");
            IJM.$.canvasPicPlain = IJM.$.canvasPlain.find(".ijm__img");

            IJM.$.overlay = IJM.$.container.find('.ijm__overlay');
            IJM.$.overlay3D = IJM.$.canvas3D.find('.ijm__overlay');
            IJM.$.overlayPlain = IJM.$.canvasPlain.find('.ijm__overlay');

            // tooltips are dynamically created after. so no them in here

            IJM.$.config = IJM.$.container.find('.ijm__config');
            IJM.$.points = IJM.$.config.find('.point');

            // not a jquery element really but let it be here
            IJM.tmpl = $.templates("#ijm__tmpl");
        };

        /**
         * Attach Events
         */
        IJM.attachEvents = function(){
            IJM.$.controls.on('click', function(){

                if ( $(this).hasClass('ijm__control--3D') ) {
                    IJM.showMap('3D', true);
                } else {
                    IJM.showMap('plain', true)
                }

                return false;
            });

            IJM.$.overlay.on('click', '.ijm__tooltip', function(e) {
                if ( !$(e.target).parents('.ijm__popup').length ) {
                    var popup = $(this).find('.ijm__popup');

                    if ( popup.hasClass('active') ) {
                        popup.removeClass('active');
                    } else {
                        IJM.openPopup( popup );
                    }

                }
                if ( $(e.target).is('a') ) {
                    return true;
                }

                return false;
            });

            IJM.$.container.on( 'click', function(e){
                if ( $(e.target).hasClass('ijm__overlay') || $(e.currentTarget).hasClass('ijm') ) {
                    IJM.closeAll();
                }
                if ( $(e.target).is('a') ) {
                    return true;
                }

                return false;
            });

            IJM.$.overlay.on('click', '.ijm__popup-close', function(){
                $(this).parents('.ijm__popup').removeClass('active');

                return false;
            });
        };

        /**
         * Open popup
         */
        IJM.openPopup = function( popup ) {
            IJM.closeAll();
            popup.addClass('active');

            var container = IJM.$.container;

            var
                PW = popup.outerWidth(),
                PL = popup.offset().left,
                WW = container.outerWidth(),
                WL = container.offset().left;

            // Проверка на уход "вправо"
            var dx = 30,
                ml = 0,
                D = (WW + WL + dx) - ( PL + PW );

            if ( D < 0 ) {
                popup.css('margin-left',  -PW/2 + D );
                popup.find('.ijm__popup-corner').css('margin-left', -D - 18);
            }

            // Проверка на уход "влево"
            var D2 = PL - (WL-dx);
            if ( D2 < 0 ) {
                popup.css('margin-left', -PW/2 - D2 );
                popup.find('.ijm__popup-corner').css('margin-left', D2 - 18);

            }

        };

        /**
         * Close all popups
         */
        IJM.closeAll = function() {
            IJM.$.overlay.find('.ijm__popup').removeClass('active');
        };

        /**
         * Show map (for changing 3d/plain map)
         */
        IJM.showMap = function(type, full_behaviour) {
            if ( !type ) {
                $.error('showMap type parameters is missed!');
            }

            if ( type == '3D' ) {

                IJM.$.container.css("height", IJM.$.canvasPic3D.height() );

                IJM.$.canvas.removeClass('active');
                IJM.$.canvas3D.addClass('active');
            } else if ( type == 'plain' ){
                IJM.$.container.css("height", IJM.$.canvasPicPlain.height() );

                IJM.$.canvas.removeClass('active');
                IJM.$.canvasPlain.addClass('active');
            } else {
                $.error('showMap wrong type parameter');
            }

            if ( full_behaviour ) {
                if ( type == '3D' ) {
                    IJM.$.control3D.addClass('active');
                    IJM.$.controlPlain.removeClass('active');
                } else if ( type == 'plain' ){
                    IJM.$.control3D.removeClass('active');
                    IJM.$.controlPlain.addClass('active');
                }
            }
        };

        /**
         * Compile Overlays
         */
        IJM.compileOverlays = function() {

            IJM.$.points.each(function(){
                var point = {},
                    $point = $(this);

                point.title = $point.attr('data-title');
                point.status = $point.attr('data-status');
                point.status_label = $point.attr('data-status-label');
                point.popup = $point.html();
                point.pw1 = $point.attr('data-pw1') || 285;
                point.pw2 = $point.attr('data-pw2') || 270;
                point.pos = $point.attr('data-pos') || 'auto';
                point.placemark_width = $point.attr('data-placemark-width') || 'auto';
                point.placemark_flagpos = $point.attr('data-placemark-flagpos') || 'auto';

                if ( $point.attr("data-x-3d") && $point.attr("data-y-3d") ) {
                    var point3D = $.extend( point, {
                        x : Number( $point.attr("data-x-3d") ),
                        y : Number( $point.attr("data-y-3d") )
                    });
                    IJM.addPointToOverlay( point3D, '3D' );
                }

                if ( $point.attr("data-x") && $point.attr("data-y") ) {
                    var pointPlain = $.extend( point, {
                        x : Number( $point.attr("data-x") ),
                        y : Number( $point.attr("data-y") )
                    });
                    IJM.addPointToOverlay( pointPlain, 'plain' );
                }
            });

        };

        IJM.addPointToOverlay = function (point, overlay_type) {
            var overlay = overlay_type == '3D' ? IJM.$.overlay3D : IJM.$.overlayPlain;
            var canvas = overlay_type == '3D' ? IJM.$.canvas3D : IJM.$.canvasPlain;

            overlay.append( IJM.tmpl.render( point ) );

            // better positioning of popup
            var tooltip_added = overlay.find('.ijm__tooltip').last();
            var popup = tooltip_added.find('.ijm__popup').length
                      ? tooltip_added.find('.ijm__popup') : false;

            if ( popup ) {

                // if PW is passed
                if ( point.pw1 ) {
                    popup.find('main').css('width', point.pw1 );
                }

                if ( point.pw2 ) {
                    popup.find('aside').css('width', point.pw2 );
                }

                if ( ( point.pw1 ) && (point.pw2 ) ) {
                    if ( popup.find("aside").length ) {
                        popup.css('width', 0 + point.pw1 + point.pw2 + 1);
                    } else {
                        popup.css('width', point.pw1  );
                    }
                }

                var w = tooltip_added.outerWidth(),
                    h = tooltip_added.outerHeight(),
                    W = popup.outerWidth(),
                    H = popup.outerHeight();

                if ( point.pos == 'auto' ) {
                    if ( (point.y + H - 200) > canvas.height() ) {
                        point.pos = 'up';
                    } else {
                        point.pos = 'down';
                    }
                }

                var top;
                if ( point.pos == 'up' ) {
                    popup.addClass('up');
                    top = -H - 18;
                } else if ( point.pos == 'down' ) {
                    top = h + 18 + 8;
                }

                popup.css({
                    'top'         : top,
                    'margin-left' : -W/2
                });

            }
        }

        IJM.init = function(){
            IJM.cacheJqueryElemsInstances();
            IJM.attachEvents();

            IJM.compileOverlays();

            IJM.showMap('3D', true);
        };

        return IJM;
    })()


    /**
     * Dummy Module Example
     */
    ,DummyModule = (function(){
        return {
            init : function() {
                // do something
            }
        }
    })()

    ;App.init();

});
