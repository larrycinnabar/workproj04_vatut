$(function() {

    var App = (function(){

        return {
            init : function() {
                FlatSearchPriceRange.init();
                FlatCustomSelects.init();
                FadeStarter.init();
                StickyMenu.init();
                FancyGallery.init();
                NewBlocksSlider.init();

                //DummyModule.init();
            }
        }
    })()

    /**
     * Sticky Menu
     */
    ,StickyMenu = (function(){

        var nav = $(".main-nav"),
            phone = $(".new-central-park").find('.phone'),
            checkpoints = [];

        return {

            calculate : function() {

                checkpoints = [];
                // Получение всех чекпоинтов
                $(".nav-checkpoint").each(function(){
                    var checkpoint_value = $(this).attr('data-nav-checkpoint');
                    if ( checkpoint_value ) {
                        checkpoints.push({
                            'y'   : $(this).offset().top,
                            'key' : checkpoint_value
                        });
                    }
                });
            },

            init : function() {

                var SM = this;
                var nc = $(".nav-checkpoint").length;
                var nav_height = nav.height();
                var scrollTopThreshold = nav.offset().top;

                SM.calculate();
                window.setTimeout(SM.calculate, 100); // повторной пересчет

                // скроллим при нажатии
                $("[rel=scrollto]").on('click', function(){
                    SM.calculate();
                    var checkpoint = $(this).attr('href').replace('#', '');
                    var top = $('.' + checkpoint).offset().top;
                    var dy = nav.hasClass('fixed') ? nav.height() : nav.height()*2;

                    $('html,body').animate({
                        scrollTop: top - dy
                    }, 400);

                    return false;
                })

                // следим за скроллом
                $(window).scroll(function(){
                    var st = $(document).scrollTop();

                    // Делаем меню фиксовым
                    if ( st >=  scrollTopThreshold ) {
                        nav.addClass('fixed');
                        phone.addClass('fixed');
                        var right = $(window).width() > 1100 ? ($(window).width() - 1100)/2 : 0;
                        phone.css({ 'right' : right} );
                    } else {
                        nav.removeClass('fixed');
                        phone.removeClass('fixed');
                        phone.attr('style', '');
                    }

                    // проверяем на чекпоинты
                    var current_key = '';
                    for (var i=0; i<nc; i++) {
                        if ( st + nav_height*2 >= checkpoints[i]['y'] ) {
                            current_key = checkpoints[i]['key'];
                        }
                    }

                    $(".main-nav").find(".main-nav__link").removeClass("active");
                    if ( current_key ) {
                        $(".main-nav").find(".main-nav__link--" + current_key).addClass('active');
                    }
                });
            }
        }
    })()

    /**
     * FadeStarter
     */
    ,FadeStarter = (function(){
        return {

            init : function() {
                window.setTimeout(function(){
                    $("#fade-starter").addClass('faded').on('transitionend', function(){
                        window.setTimeout(function(){
                            $("#fade-starter").remove();
                        }, 500);
                    });
                }, 100);
            }
        }
    })()

    /**
     * Flat Custom Selects
     */
    ,FlatCustomSelects = (function(){
        return {
            init : function() {
                $('.flat-search__form-select').simpleselect();
            }
        }
    })()

    /**
     * New Block Slider
     */
    ,NewBlocksSlider = (function(){
        return {
            init : function() {
                var n = $(".newblocks__dot").length;

                $(".newblocks__dot").on('click', function(){

                    $(".newblocks__dot").removeClass('newblocks__dot--active');
                    for (var i=0; i<n; i++) {
                        $(".newblocks").removeClass( 'newblocks--bg' + (i+1) );
                    }
                    $(".newblocks").addClass('newblocks--bg' + Number($(this).index() + 1) );
                    $(this).addClass('newblocks__dot--active');

                    return false;
                });
            }
        }
    })()

    /**
     * FlatSearchPriceRange
     */
    ,FlatSearchPriceRange = (function(){
        return {
            init : function() {
                jQuery("#price_range").slider({
                    from      : 3939454,
                    to        : 9874123,
                    step      : 1000,
                    smooth    : true,
                    round     : 0,
                    dimension : ".-",
                    limits    : false
                 });
            }
        }
    })()


    /**
     * FancyGallery
     */
    ,FancyGallery = (function(){
        return {
            init : function() {

                //  клик по большой картинке это клик по первой превьюшке
                $(".fancygallery__caller").on('click', function(){
                    $(".fancybox-thumb").eq(0).click();
                    return false;
                });

                // thumb actions
                var ThumbShifter = (function(){
                    var TS = {};
                    TS.goLeft = function ( $ul ) {
                        var pos = Number( $ul.attr('data-pos') );

                        if ( pos == 0 ) {
                            ThumbShifter.goTo( $ul, max_pos-n_pos );
                        } else {
                            $ul.removeClass( 'pos-' + pos );
                            $ul.addClass( 'pos-' + Number(pos - 1) );
                        }

                        pos -= 1;
                        $ul.attr('data-pos', pos);
                    };
                    TS.goRight = function ( $ul ) {
                        var pos = Number( $ul.attr('data-pos') );
                        var t = max_pos-n_pos;

                        if ( pos == t ) {
                            ThumbShifter.goTo($ul, 0);
                        } else {
                            $ul.removeClass( 'pos-' + pos );
                            $ul.addClass( 'pos-' + Number(pos + 1) );
                        }

                        pos += 1;
                        $ul.attr('data-pos', pos);
                    };
                    TS.goTo = function( $ul, pos_goto ) {
                        var pos = Number( $ul.attr('data-pos') );

                        $ul.removeClass( 'pos-' + pos );
                        $ul.addClass( 'pos-' + Number(pos_goto) );
                        $ul.attr('data-pos', Number(pos_goto) );
                    }

                    return TS;
                })();

                var max_pos = $(".fancygallery__preview").length,
                    n_pos = 5;

                jQuery(".fancybox-thumb").fancybox({
                    padding: 0,

                    prevEffect	: 'none',
                    nextEffect	: 'none',
                    helpers	: {
                        title	: {
                            type: 'inside'
                        },
                        thumbs	: {
                            width	: 100,
                            height	: 100
                        },
                        overlay: {
                           locked: false
                         }
                    },
                    afterLoad: function() {
                          this.title = '<div class="title-extra-wrapper">' + this.title + '</div>';
                    },

                    afterShow: function () {

                        // At first time - draw controls
                        if ($(".fancybox-thumb-control").length == 0) {
                            setTimeout(function () {
                                $("<a href='#' class='fancybox-thumb-control prev'><span></span></a>").insertAfter("#fancybox-thumbs");
                                $("<a href='#' class='fancybox-thumb-control next'><span></span></a>").insertAfter("#fancybox-thumbs");

                                var $ul = $("#fancybox-thumbs").find('ul');
                                $ul.attr('data-pos', 0);
                                $ul.addClass('pos-0');

                            }, 100);
                        };

                        // Move thumbs if needed
                        setTimeout(function(){
                            var $t   = $("#fancybox-thumbs"),
                                $ul  = $t.find('ul'),
                                $lis = $t.find("li"),
                                $lia = $t.find("li.active"),
                                idx  = Number( $lis.index( $lia ) ),
                                pos  = Number( $ul.attr('data-pos') );

                            var visible_l = pos, visible_r = pos + 4;
                            var dx = 0;
                            if ( pos > max_pos ) pos = max_pos;

                            if ( idx > visible_r ) {
                                dx = idx - visible_r;
                            } else if ( idx < visible_l ) {
                                dx = idx -  visible_l;
                            } else {
                                dx = 0;
                            }

                            if ( dx ) ThumbShifter.goTo( $ul, pos + dx);
                        }, 110);

                    },
                    afterClose: function () {
                        $(".fancybox-thumb-control").remove();
                    }
                });

                // thumb buttons behaviour
                $(document).on('click', ".fancybox-thumb-control", function(){
                    var $ul = $("#fancybox-thumbs").find('ul');
                    if ( $(this).hasClass('disabled') ) {
                        return false;
                    }

                    if ( $(this).hasClass('prev') ) {
                        // go left
                        ThumbShifter.goLeft ( $ul );
                    } else {
                        // go right
                        ThumbShifter.goRight ( $ul );
                    }
                    return false;
                });
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,DummyModule = (function(){
        return {
            init : function() {
                // do something
            }
        }
    })()

    ;App.init();

});


jQuery.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};
